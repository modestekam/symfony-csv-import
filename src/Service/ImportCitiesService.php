<?php

namespace App\Service;

use App\Entity\City;
use App\Repository\CityRepository;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Reader;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportCitiesService
{
    public function __construct(
        readonly private CityRepository $cityRepository,
        readonly private EntityManagerInterface $em
    )
    {
    }
    public function importCities(SymfonyStyle $io) : void
    {
        $io->title('Importation des viles');

        $cities = $this->readCsvFile();

        $io->progressStart(count($cities));

        $i = 0;
        foreach ($cities as $arrayCity){

            $city = $this->ceateOrUpdateCity($arrayCity);
            $this->em->persist($city);

            if (($i % 50) == 0){
                $this->em->flush();
                $this->em->clear();
            }

            $i++;

            $io->progressAdvance();
        }

        $this->em->flush();

        $io->progressFinish();

        $io->success('Importation terminée');
    }

    private function readCsvFile() : Reader
    {
        $csv = Reader::createFromPath('%kernel.root_dir%/../import/cities.csv', 'r');
        $csv->setHeaderOffset(0);

        return $csv;
    }

    private function ceateOrUpdateCity(array $arrayCity): City
    {
        $city = $this->cityRepository->findOneBy(['inseeCode' => $arrayCity['insee_code']]);

        if(!$city){
            $city = new City();
        }

        return $city
            ->setInseeCode($arrayCity['insee_code'])
            ->setCityCode($arrayCity['city_code'])
            ->setZipCode($arrayCity['zip_code'])
            ->setLabel($arrayCity['label'])
            ->setLatitude($arrayCity['latitude'])
            ->setLongitude($arrayCity['longitude'])
            ->setDepartmentName($arrayCity['department_name'])
            ->setDepartmentNumber($arrayCity['department_number'])
            ->setRegionName($arrayCity['region_name'])
            ->setRegionGeoJsonName($arrayCity['region_geojson_name'])
            ->setCountryCode($arrayCity['country_code'])
        ;
    }
}